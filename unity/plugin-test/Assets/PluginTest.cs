﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Runtime.InteropServices;

[RequireComponent(typeof(FilenameFinder))]
public class PluginTest : MonoBehaviour
{
    private int[] vertices;
    GCHandle verticesHandle;
    public Text textToChange;
    public int vertexLength = 200000;
    public Object testPathObject;
    public string testPathFilename;

    
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)] 
    public delegate void MyDelegate(string str);
    public static void CallBackFunction(string str) {
        Debug.Log("Call from plugin : " + str);
    }
    
    void Start()
    {
        vertices = new int[vertexLength];
        verticesHandle = GCHandle.Alloc(vertices, GCHandleType.Pinned);

        // Setup callback function
        MyDelegate callback_delegate = new MyDelegate( CallBackFunction );
        System.IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
        PluginBridge.SetDebugFunction( intptr_delegate );
    }

    // Update is called once per frame
    void Update()
    {
        string text = "" + PluginBridge.foo(4, 5);
        Debug.Log("foo test: " + text);

        string text2 = "" + PluginBridge.testMemoryShare(verticesHandle.AddrOfPinnedObject(), vertexLength);
        Debug.Log("plugin-test testMemoryShare return value: " + text2);
        int sum = 0;
        foreach (int v in vertices) {
            sum += v;
        }

        string path = Application.streamingAssetsPath + "/" + testPathFilename;

        if (path.StartsWith("jar"))
        {
            WWW www = new WWW(path);

            if (string.IsNullOrEmpty(www.error)) {
                path = Application.persistentDataPath + "/" + testPathFilename;

                if (!System.IO.File.Exists(path)) {
                    System.IO.FileStream fs = System.IO.File.Create(path);
                    fs.Write(www.bytes, 0, www.bytesDownloaded);
                    fs.Dispose();
                    Debug.Log("copied file");
                }
                else {
                    Debug.Log("file already exists");
                }
            }
            else {
                Debug.Log("Error copying asset: " + path);
                Debug.Log("www: " + www.error);
            }
        }

        int testFilePathResult = PluginBridge.testFilePath(path);
        Debug.Log("file path test: " + testFilePathResult);
        textToChange.text = text + " " + sum + " " + " " + testFilePathResult + path;
        // }
    }

    void OnDestroy() {
        verticesHandle.Free();
    }
}
