﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class PluginBridge : MonoBehaviour
{
     #if UNITY_EDITOR
    [DllImport ("libEngine", CallingConvention = CallingConvention.Cdecl)]
    #else
    [DllImport ("von_lied", CallingConvention = CallingConvention.Cdecl)]
    #endif
    public static extern int foo(int a, int b);
    [DllImport ("libEngine", CharSet = CharSet.Ansi, BestFitMapping = false, ThrowOnUnmappableChar = true)]
    public static extern int testMemoryShare(System.IntPtr array, int len);
    [DllImport ("libEngine", CharSet = CharSet.Ansi, BestFitMapping = false, ThrowOnUnmappableChar = true)]
    public static extern int testFilePath([MarshalAs(UnmanagedType.LPStr)] string path);
    [DllImport ("libEngine", CharSet = CharSet.Ansi, BestFitMapping = false, ThrowOnUnmappableChar = true)]
    public static extern void SetDebugFunction( System.IntPtr fp );
}
