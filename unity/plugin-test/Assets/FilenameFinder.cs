using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Runtime.InteropServices;

[ExecuteInEditMode]
[RequireComponent(typeof(PluginTest))]
public class FilenameFinder : MonoBehaviour
{
    void Update() {
        PluginTest pluginTest = GetComponent<PluginTest>();
        if (pluginTest.testPathObject)
            pluginTest.testPathFilename = pluginTest.testPathObject.name;
    }
}
