// #include <iostream.h>
#include <stdio.h>
#include <stdlib.h>

void (*DebugLog)(char*) = NULL;

extern "C"
{
    int testMemoryShare( int *a, int len) {
        for (int i = 0; i < len; i++) a[i] = a[i] + 1;
        return a[1];
    }

    int foo( int a, int b) {
        return a + b;
    }

    int testFilePath( char * str) {
        FILE * fPtr;

        char ch;
        fPtr = fopen(str, "r");

        if(fPtr == NULL)
        {
            return -23;
        }
        ch = fgetc(fPtr);
        fclose(fPtr);
        DebugLog(str);

        return (int)ch;
    }

    void SetDebugFunction( void (*fun_ptr)(char*)) {
        DebugLog = fun_ptr;
    }
}
